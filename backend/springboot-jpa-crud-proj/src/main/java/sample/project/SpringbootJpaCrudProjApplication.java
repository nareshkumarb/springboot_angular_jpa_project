package sample.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJpaCrudProjApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootJpaCrudProjApplication.class, args);
	}

}
