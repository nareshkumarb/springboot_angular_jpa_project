package sample.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sample.project.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
